const button = document.querySelector('.btn');
const wrapper = document.querySelector('.wrapper');
button.addEventListener('click', function (event) {
    const diameter = prompt('Введіть діаметр кола');
    button.classList.add('invisible');
    for (let i = 0; i < 100; i++) {
        const circle = document.createElement('div');
        const firstСolor = getRandomInt(0, 255);
        const secondСolor = getRandomInt(0, 255);
        const thirdСolor = getRandomInt(0, 255);
        const svg = `<svg xmlns="http://www.w3.org/2000/svg" version="1.0" width="${diameter}" height="${diameter}">
        <circle cx="${diameter/2}" cy="${diameter/2}" r="${diameter/2}" fill="rgb(${firstСolor},${secondСolor}, ${thirdСolor})" />
        </svg>`
        circle.innerHTML = svg;
        wrapper.append(circle);
    }
})

wrapper.addEventListener('click', function (event) {
if(event.target.tagName === 'circle'){
    event.target.closest("div").classList.add('invisible');
}
})

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}